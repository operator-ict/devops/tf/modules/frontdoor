resource "azurerm_cdn_frontdoor_origin_group" "group" {
  name                     = var.name
  cdn_frontdoor_profile_id = var.frontdoor_profile_id
  session_affinity_enabled = false

  dynamic "health_probe" {
    for_each = var.spoke.probe.enable ? { probe_disabled = true } : {}

    content {
    interval_in_seconds = var.spoke.probe.interval_in_seconds
    path                = var.spoke.probe.path
    protocol            = var.spoke.probe.protocol
    request_type        = var.spoke.probe.request_type
    }
  }

  load_balancing {
    sample_size                        = 4
    successful_samples_required        = 3
    additional_latency_in_milliseconds = 50
  }
}

resource "random_string" "endpoint_suffix" {
  length  = 3
  special = false
  upper   = false
  keepers = {
    name = var.name
  }
}

resource "azurerm_cdn_frontdoor_endpoint" "endpoint" {
  name                     = "${var.name}-${random_string.endpoint_suffix.result}"
  cdn_frontdoor_profile_id = var.frontdoor_profile_id
  enabled                  = true
}

resource "azurerm_cdn_frontdoor_origin" "origins" {

  cdn_frontdoor_origin_group_id  = azurerm_cdn_frontdoor_origin_group.group.id
  name                           = var.name
  host_name                      = var.spoke.origin.hostname
  certificate_name_check_enabled = false
  enabled                        = true
}

# Get dns zone like substring of domain
locals {
  domain_split = merge(flatten([
  for name, route in var.spoke.routes : {
  for domain, config in route.domains : domain => merge(
    (
      try(config.is_dns_zone, false) ?
        {domain_zone: domain, domain_name: domain } :
        regex("(?P<domain_name>[^.]+)\\.(?P<domain_zone>.+\\.[^.]+)", domain)
      ), {  route: name,
            is_dns_zone : try(config.is_dns_zone, false),
            dns_traffic: try(config.dns_traffic, true),
            dns_auth: try(config.dns_auth, true)
          }
    )
  }
  ])...)

  dns_zone_names = setsubtract(toset(distinct([
  for d in local.domain_split : d.domain_zone
  ])), toset(var.exclude_dns_zones_list))
}

data "azurerm_dns_zone" "zones" {
  for_each            = local.dns_zone_names
  name                = each.value
  resource_group_name = try(var.dns_zones_resource_group_name, null)
}

output "debug" {
  value = {
    #  domain_split = local.domain_split
    #  domain_zones = local.dns_zone_names
    #  zones        = data.azurerm_dns_zone.zones
  }
}

module "frontdoor_custom_domains" {
  for_each = local.domain_split

  source                      = "gitlab.com/operator-ict/frontdoor-custom-domain/azurerm"
  version                     = "0.0.1"
  frontdoor_profile_id        = var.frontdoor_profile_id
  name                        = each.key
  dns_zone_id                 = contains(keys(data.azurerm_dns_zone.zones), each.value.domain_zone) ? data.azurerm_dns_zone.zones[each.value.domain_zone].id : null
  dns_zone_name               = each.value.domain_zone
  domain_name                 = each.value.domain_name
  is_dns_zone                 = each.value.is_dns_zone
  dns_auth                    = try(each.value.dns_auth, true)
  dns_traffic                 = try(each.value.dns_traffic, true)
  frontdoor_endpoint_hostname = azurerm_cdn_frontdoor_endpoint.endpoint.host_name
  frontdoor_endpoint_id       = azurerm_cdn_frontdoor_endpoint.endpoint.id
  resource_group_name         = var.dns_zones_resource_group_name
}

resource "azurerm_cdn_frontdoor_route" "route" {
  for_each = var.spoke.routes

  name                          = each.key
  cdn_frontdoor_endpoint_id     = azurerm_cdn_frontdoor_endpoint.endpoint.id
  cdn_frontdoor_origin_group_id = azurerm_cdn_frontdoor_origin_group.group.id
  cdn_frontdoor_origin_ids      = [azurerm_cdn_frontdoor_origin.origins.id]
  cdn_frontdoor_rule_set_ids    = each.value.rule_set_id
  enabled                       = true

  forwarding_protocol    = "HttpsOnly"
  https_redirect_enabled = true
  patterns_to_match      = ["/*"]
  supported_protocols    = ["Http", "Https"]

  cdn_frontdoor_custom_domain_ids = [for domain in keys(each.value.domains) : module.frontdoor_custom_domains[domain].domain_id]
  link_to_default_domain          = try(length(keys(each.value.domains)) == 0, false)

  dynamic "cache" {
    for_each = tobool(each.value.cache.enabled) ? [1] : []
    content {
      query_string_caching_behavior = "UseQueryString"
      #https://learn.microsoft.com/en-us/azure/frontdoor/front-door-rules-engine-actions?tabs=portal&pivots=front-door-standard-premium#properties
      query_strings                 = []
      compression_enabled           = true
      content_types_to_compress     = [
        "application/eot", "application/font", "application/font-sfnt", "application/javascript", "application/json",
        "application/opentype", "application/otf", "application/pkcs7-mime", "application/truetype",
        "application/ttf", "application/vnd.ms-fontobject", "application/xhtml+xml", "application/xml",
        "application/xml+rss", "application/x-font-opentype", "application/x-font-truetype", "application/x-font-ttf",
        "application/x-httpd-cgi", "application/x-javascript", "application/x-mpegurl", "application/x-opentype",
        "application/x-otf", "application/x-perl", "application/x-ttf", "font/eot", "font/ttf", "font/otf",
        "font/opentype", "image/svg+xml", "text/css", "text/csv", "text/html", "text/javascript", "text/js",
        "text/plain", "text/richtext", "text/tab-separated-values", "text/xml", "text/x-script", "text/x-component",
        "text/x-java-source"
      ]
    }
  }

  depends_on = [module.frontdoor_custom_domains]
}

#resource "azurerm_cdn_frontdoor_custom_domain_association" "association" {
#  for_each = toset(["dnsch.rabin.golemio.cz"])
##  for_each = var.spoke.routes["strict-tls"].domains
#
#  cdn_frontdoor_custom_domain_id = "/subscriptions/b890c52d-e3e8-4a1e-98c1-b7ab1665b2a1/resourceGroups/rg-hub-central-frontdoor/providers/Microsoft.Cdn/profiles/frontdoor/customDomains/domain-dnsch-rabin-golemio-cz" #module.frontdoor_custom_domain[each.key].domain_id
#  cdn_frontdoor_route_ids        = ["/subscriptions/b890c52d-e3e8-4a1e-98c1-b7ab1665b2a1/resourceGroups/rg-hub-central-frontdoor/providers/Microsoft.Cdn/profiles/frontdoor/afdEndpoints/rabin-1ca/routes/strict-tls"]
#}
