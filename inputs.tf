variable "name" {
  type = string
}

variable "frontdoor_profile_id" {
  type = string
}

variable "spoke" {
  type = object({
    probe = optional(object({
      enable              = optional(bool,false)
      interval_in_seconds = optional(number,240)
      path                = optional(string,"/healthz")
      protocol            = optional(string,"Http")
      request_type        = optional(string,"GET")
    }),{ enable = false })
    origin = object({
      hostname = string
    })
    routes = map(object({
      domains     = map(map(string))
      cache       = object({ enabled : bool })
      rule_set_id = optional(list(string),[])
    }))
  })
}

variable "resource_group_name" {
  type = string
}
variable "dns_zones_resource_group_name" {
  type = string
}

variable "exclude_dns_zones_list" {
  type = list(string)
  default = []
}
