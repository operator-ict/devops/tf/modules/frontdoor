# Unreleased
## Added
## Changed
## Fixed

# [0.2.3] - 2024-03-25
## Added
- adding rule set option
## Changed
## Fixed

# [0.2.2] - 2024-03-25
## Added
## Changed
## Fixed
- fix dns zone name
## Note
- PS: Nepodařilo se odebrat DnsZoneId ani pres API, ani pres TF, tak zatim to tam nechame a uvidime jestli to dela problem

# [0.2.1] - 2024-03-25
## Added
- allow exclude domain zones
## Changed
## Fixed
